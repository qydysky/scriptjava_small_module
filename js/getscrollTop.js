function getscrollTop(){
    var y;
    if(window.pageYOffset){
        y = window.pageYOffset;
    } else if(document.documentElement && document.documentElement.scrollTop){
        y = document.documentElement.scrollTop;
    } else if(document.body) {
        y = document.body.scrollTop;
    }
    return y;
}
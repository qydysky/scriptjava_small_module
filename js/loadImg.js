/**
 * @author qydysky
 * @version 20181122
 * @function loadimgfu 图片懒加载
 */
function loadimgfu() {}
loadimgfu.prototype={
    check:0,
    time:0,
    load_sign:0,
    img_tim:0,
    fu:function(val){
        switch (val.type){
            case "DOMContentLoaded":
                img(1);
                break;
            case "load":
                loadimgfu.load_sign=1;
                backgroung_img();

                /*减少回流*/
                if(loadimgfu.time)clearTimeout(loadimgfu.time);
                loadimgfu.time=setTimeout(function () {
                    img(0);
                },500);

                dom_change();
                break;
            case "dom_change":
            case "visibilitychange":
                /*减少回流*/
                if(loadimgfu.time)clearTimeout(loadimgfu.time);
                loadimgfu.time=setTimeout(function () {
                    img(0);
                },500);
                break;
            case "scroll":
                if(!loadimgfu.load_sign)break;
                /*减少回流*/
                if(loadimgfu.time)clearTimeout(loadimgfu.time);
                loadimgfu.time=setTimeout(function () {
                    img(0);
                },55);
                break;
            default:
                break;
        }



        function dgt(e){
            return document.getElementsByTagName(e);
        }

        function loadfu(i){
            dgt("img")[i].setAttribute("src",dgt("img")[i].getAttribute("data-src"));
            dgt("img")[i].removeAttribute("data-src");
        }

        function backgroung_img() {
            for(var i=0;dgt("div")[i]||dgt("i")[i];i++) {
                if(dgt("div")[i]&&dgt("div")[i].getAttribute("b-src")){
                    var b_img_div = dgt("div")[i].getAttribute("b-src");
                    if (b_img_div) dgt("div")[i].setAttribute("style", "background-image:url('" + b_img_div + "');");
                }
                if(dgt("i")[i]&&dgt("i")[i].getAttribute("b-src")){
                    var b_img_i = dgt("i")[i].getAttribute("b-src");
                    if (b_img_i) dgt("i")[i].setAttribute("style", "background-image:url('" + b_img_i + "');");
                }
            }
        }

        function img(e) {

            if (e) {
                for(var i=0;dgt("img")[i];i++)
                if (!dgt("img")[i].getAttribute("data-src")) {
                    dgt("img")[i].setAttribute("data-src", dgt("img")[i].getAttribute("src"));
                    dgt("img")[i].removeAttribute("src");
                }
                return;
            }

            function isElementInViewport (el) {
                var rect = el.getBoundingClientRect();
                return {
                    bottom:rect.bottom >= 0,
                    top:rect.top <= (window.innerHeight || document.documentElement.clientHeight),
                };
            }


            var v=0;
            var load_in_this_trun=3;
            var lastdom;

            function forload2() {
                loadimgfu.prototype.check=1;
                if(!dgt("img")[v]){
                    loadimgfu.prototype.check=0;
                    return;
                }
                
                if(dgt("img")[v].getAttribute("data-src")&&dgt("img")[v].parentElement.className!=lastdom){
                    var size = isElementInViewport(dgt("img")[v]);
                    if (!size.top) {
                        lastdom = dgt("img")[v].parentElement.className;
                        var o = (window.getComputedStyle(dgt("img")[v].parentNode).getPropertyValue("overflow-y") || window.getComputedStyle(dgt("img")[v].parentNode).getPropertyValue("overflow"));
                        if (!o) {
                            loadimgfu.prototype.check = 0;
                            return;
                        }
                    } else {
                        if (size.bottom) {
                            loadfu(v);
                            load_in_this_trun = 0;
                        }
                    }
                }
                v++;
                if(!dgt("img")[v]){
                    loadimgfu.prototype.check=0;
                    return;
                }
                if(load_in_this_trun){
                    load_in_this_trun--;
                    forload2();
                    return;
                }
                load_in_this_trun=3;
                if(loadimgfu.prototype.img_tim)cancelAnimationFrame(loadimgfu.prototype.img_tim);
                loadimgfu.prototype.img_tim=requestAnimationFrame(forload2);
            }
            if(loadimgfu.prototype.img_tim)cancelAnimationFrame(loadimgfu.prototype.img_tim);
            loadimgfu.prototype.img_tim=requestAnimationFrame(forload2);
        }

        function dom_change() {
            var timer;
            var callback = function (records){
                records.map(function(record){
                    if(record.addedNodes.length){
                        var e=record.addedNodes[0];
                        if(e.nodeName=="IMG"){
                            call();
                            return;
                        }

                        function check_img(e) {
                            if(e.children&&e.children.length){
                                for(var i=0;e.children[i];i++){
                                    if(e.children[i].nodeName=="IMG"){
                                        call();
                                        return 1;
                                    }

                                    if(e.children[i].children&&e.children[i].children.length){
                                        var r=check_img(e.children[i]);
                                        if(r)return;
                                    }
                                }
                            }
                        }
                        check_img(e);

                        function call() {
                            if(timer)clearTimeout(timer);
                            timer=setTimeout(function () {
                                if(loadimgfu.prototype.check==1){
                                    call();
                                    return;
                                }
                                loadimgfu.prototype.fu({type:"dom_change"});
                            },1000);
                            return;
                        }
                    }
                });
            };

            var dom_ch = new MutationObserver(callback);

            var option = {
                'childList': true,
                'subtree': true
            };
            dom_ch.observe(document, option);
        }
    },
};
var loadimg=new loadimgfu();

document.addEventListener("DOMContentLoaded", function(e) {
    loadimg.fu(e);
});
window.addEventListener("scroll",function (e) {
    loadimg.fu(e);
});
document.addEventListener("visibilitychange", function(e) {
    loadimg.fu(e);
});

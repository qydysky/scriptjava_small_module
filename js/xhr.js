/**
 * @function xhr xhr请求
 * @version 0.0
 * @param {*} url 地址
 * @param {*} callback 回调，为数组
 */
function xhr(url,callback){
    var xhr=new XMLHttpRequest();
    xhr.onreadystatechange=function(){
        switch (xhr.readyState){
            case 0:
                if(callback[0])callback[0](xhr);
                break;
            case 1:
                if(callback[1])callback[1](xhr);
                break;
            case 2:
                if(callback[2])callback[2](xhr);
                break;
            case 3:
                if(callback[3])callback[3](xhr);
                break;
            case 4:
                if(callback[4])callback[4](xhr);
                break;
            default:
                break;
        };
    }
    xhr.open("GET",url);
    xhr.send();
}
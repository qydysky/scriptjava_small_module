/**
 * @function 将B转换为KB,MB,GB
 * @version 0.0
 * @param i 输入的数值，单位B
 * @param bit 输出的小数位数
 * @returns 转换好的值
 */
function bite(i,bit){/*B*/
    var o=i;
    return ([
          (i/=1024)>1024?
          (
                (i/=1024)>1024?
                (i/1024).toFixed(bit):
                i.toFixed(bit)
          ):
          i.toFixed(bit)
          ,
          (o/=1024)>1024?
          (
                (o/=1024)>1024?"G":"M"
          ):"K"
    ]);
}
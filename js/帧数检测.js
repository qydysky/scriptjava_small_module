/**
 * @author qydysky
 * @description fps检测
 * @version 20181224
 */
(function () {
    var fps=0,
        fpseer,
        fpser;
    function oo() {
        fps++;
        fpser=requestAnimationFrame(oo);
    }
    function pp(){
        if(parseInt(fps/2)<30)stop();
        fps=0;
        fpseer=0;
    }
    function stop(){
        console.warn("浏览器帧数过低！")
        todo();
        cancelAnimationFrame(fpser);
        clearInterval(fpseer);
    }
    function todo(){
        var e=document.getElementsByTagName("IMG");
        for(var i=0;i<e.length;i++){
            if(!e[i].src.match("gif"))continue;
            e[i].src=e[i].src.replace(/gif/,"jpg");
        }
    }
    fpseer=setInterval(pp,2000);
    fpser=requestAnimationFrame(oo);
})();
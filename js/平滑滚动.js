/**
 * @author qydysky
 * @version 20190506
 * @description 使页面滚动平滑
 */
(function () {
    var t,
        default_height=21;

    var re=0.7,//滚动主衰减比例
        de=0.05,//副衰减比例
        limi=10,//滚动数组个数
        rep=3;//连续滚动平滑度

    var passiveSupported = false;
    try {
        var options = Object.defineProperty({}, "passive", {
            get: function() {
                passiveSupported = true;
            }
        });
        window.addEventListener("test", null, options);
    } catch(err) {}

    window.addEventListener("wheel",function(e){
        var s=e.deltaY/2;
        if(Math.abs(e.deltaY)<default_height){
            s=e.deltaY<0?-default_height:default_height;
        }
        e.preventDefault();
        var el=e.target,
            old;
        while (el){
            old=el.scrollTop;
            if(!el.scrollTop)el.scrollTop+=2;/*edge上需要加2*/
            else el.scrollTop-=2;
            if(Math.abs(el.scrollTop-old)>1){
                if(el.parentNode&&el.parentNode.parentNode&&window.getComputedStyle(el.parentNode).getPropertyValue('overflow')=="hidden");
                else break;
            }
            el=el.parentNode;
        }

        slip(s,el);
    },passiveSupported ? { passive: false } : false);

    document.addEventListener("load",function () {
        default_height=getScrollLineHeight();
    });


    function getScrollLineHeight() {
        var r;
        var iframe = document.createElement('iframe');
        iframe.src = '#';
        document.body.appendChild(iframe);
        var iwin = iframe.contentWindow;
        var idoc = iwin.document;
        idoc.open();
        idoc.write('<!DOCTYPE html><html><head></head><body><span>a</span></body></html>');
        idoc.close();
        var span = idoc.body.firstElementChild;
        r = span.offsetHeight;
        document.body.removeChild(iframe);
        return r;
    }

    var tool={
        dlist:new Array(),
        list:new Array(),
        main:function(val){
            if(val<0)val=-val;
            while(val>1){
                val=parseInt(val);
                this.list.unshift(val);
                while (rep){
                    this.dlist.push(val);
                    rep--;
                }
                rep=2;
                val*=re;
                re-=de;
                limi--;
                if(re<=0||limi<0||val<1)break;
                this.list.push(parseInt(val));

            }
        }
    };

    function slip(s,e){
        try{
            if(e.parentNode.style.overflow==="hidden")return;
        }catch (e) {}


        if(!tool.list.length)tool.main(s);
        var list=t?tool.dlist.slice():tool.list.slice(),num=0;
        function move() {
                e.scrollTop+=s>0?list[num]:-list[num];
                num++;
                if(list[num]){
                    if(t)cancelAnimationFrame(t);
                    t=requestAnimationFrame(move);
                }else t=0;
        }
        if(t)cancelAnimationFrame(t);
        t=requestAnimationFrame(move);
    }
})();

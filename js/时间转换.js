/**
 * @function time 由过去某时的new Date().getTime()到现在的差值得出日期
 * @version 0.0
 * @param oldTime 旧时间，为过去某时的new Date().getTime()返回值
 * @returns 距今时间
 */
function time(oldTime){
    var time=new Date().getTime()-oldTime;
    return (
          (time/=1000)>60?/*m?*/
          (
                (time/=60)>60?/*h?*/
                (
                      (time/=60)>24?/*d?*/
                      ((time/=24).toFixed(0)+"天"):
                      (time.toFixed(0)+"小时")
                ):
                (time.toFixed(0)+"分钟")
          ):
          (time.toFixed(0)+"秒")
    );
}

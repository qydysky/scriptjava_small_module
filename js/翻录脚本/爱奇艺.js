/**
 * @author qydysky
 * @version 20181113_1.0
 * @description 爱奇艺录屏辅助脚本，运行后5s内点击录制,搭配obs使用
 * 
 * document.getElementsByTagName("video")[0].webkitEnterFullScreen();
 * 
 * css
 * video::-webkit-media-controls { 
 *   display:none !important; 
 *} 
 */
(function(){
	
	var timestep=0,
		old_p=0,
		all_p=0,
		avg_p=0,
		cc=0,
		oo;

	oo=setInterval(function(){
		if(!document.getElementsByClassName("iqp-progress-play")[0]){
			console.log("等待运行");
			return;
		}

		if(document.getElementsByTagName("video")[0].paused){
			if(timestep>10){
				console.log("暂停，等待运行");
				return;
			}else{
				if(timestep==0)console.log("开始运行");
				if(timestep<10)console.log("还有"+(10-timestep)+"s");
				else document.getElementsByTagName("video")[0].click();
				timestep++;
				return;
			}
		}else{
			if(timestep==0)document.getElementsByTagName("video")[0].click();
		}

		timestep++;

		var p=(document.getElementsByClassName("iqp-progress-play")[0]?document.getElementsByClassName("iqp-progress-play")[0].style.width:"0%");
		p=parseFloat(p.slice(0,-1));


		if(p+2*avg_p>100){
			document.getElementsByTagName("video")[0].playbackRate=0.1;
			console.log("结束。");
			clearInterval(oo);
			return;
		}

		console.log("播放长度:"+p);

		if(timestep>111)timestep=11;

		if(old_p!=0){
			cc++;
			if(cc<10){
				all_p+=p-old_p;
				avg_p=all_p/cc;
			}else{
				if(p-old_p<=5*avg_p){
					all_p+=p-old_p;
					avg_p=all_p/cc;
					all_p-=avg_p;
				}
				cc--;
			}
		}
		old_p=p;
		
	},1000);
})();
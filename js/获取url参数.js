/**
 * @function parmfu 获取url参数
 * @version 1.0
 * @param item url参数名 
 * @param defaul 失败时默认值 
 * @returns 对应值
 */
function parmfu(item, defaul) {
    if (!item) return;
    var url = window.location.search.match(/(?![?|&]).*?(?=(&|$))/g),
      ii = new Array(2),
      io;
    for (var i = 0; i < url.length || url[i].length; i++) {
      ii = url[i].match(/([^=]+)/g);
      if (!ii || !ii[0] || io) break;
      io = ii[0] === item ? ii[1] : undefined;
    }
    return io ? io : defaul;
  }